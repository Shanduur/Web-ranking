#pragma once

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

//************************************************ DEBUG ************************************************


namespace new_debug
{
	int debugIterator = 0;

	void debug(std::string output)
	{
#if DEBUG == true
		std::cout << output << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(300));
#endif
	}

	void debug(std::string var0, int var1, int var2)
	{
#if DEBUG == true
		std::cout << var0 << " " << var1 << " " << var2 << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(300));
#endif
	}

	void debug(int var0)
	{
#if DEBUG == true
		std::cout << var0 << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(300));
#endif
	}

	void debug(std::string var0, int var1)
	{
#if DEBUG == true
		std::cout << var0 << " " << var1 << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(300));
#endif
	}

	void debug()
	{
#if DEBUG == true
		std::cout << std::endl;
#endif
	}

	void reset()
	{
#if DEBUG == true
		debugIterator = 0;
#endif
	}
};


