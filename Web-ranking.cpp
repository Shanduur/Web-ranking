/*
*	Project.cpp
*
*	Created by Mateusz Urbanek on 13/03/2018.
*
*	(c) 2017 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#define DEBUG false
/*
*	Defining Debuging mode, just for testing purposes.
*
*/

#include <iostream>
/*
*	Header that defines the standard input/output stream objects
*	(source: http://www.cplusplus.com/reference/iostream/).
*
*/
#include <string>
/*
*	This header introduces string types, character traits and a set of converting functions
*	(source: http://www.cplusplus.com/reference/string/).
*
*/
#include <fstream>
/*
*	Header providing file stream classes
*	(source: http://www.cplusplus.com/reference/fstream/).
*
*/

#include <algorithm>
/*
*	This header defines a collection of functions especially designed to be used on ranges of elements.
*	A range is any sequence of objects that can be accessed through iterators or pointers,
*	such as an array or an instance of some of the STL containers. 
*	Notice though, that algorithms operate through iterators directly on the values, 
*	not affecting in any way the structure of any possible container (it never affects the size or storage allocation of the container).
*	(source: http://www.cplusplus.com/reference/algorithm/).
*
*	As for now it is not used.
*	
*/

#include <chrono>
/*
*	Header with time library, allows to use three concepts of time
*	functions - Durations, Time points and Clocks.
*	(source: http://www.cplusplus.com/reference/chrono/).
*
*/
#include <thread>
/*
*	Class to represent individual threads of execution.
*	A thread of execution is a sequence of instructions that can be executed concurrently
*	with other such sequences in multithreading environments, while sharing a same address space.
*	(source: http://www.cplusplus.com/reference/thread/).
*
*/

#include "new_debug.hpp"

//************************************************ CLASS ************************************************

class data
{
private:
	struct stats
	{
		int views = 0;
		int pc_id = 0;
		std::string filename;
	};
	/*
	*	Struct contains data read from log file
	*	-> number of views stored as integer
	*	-> identification number of PC stored as integer
	*	-> string with name of file
	*
	*/

public:
	void f_open();
	void f_close();
	void print_records1();
	void print_records1(int max);
	void print_records2();
	void setup();

	data();		/*	Constructor	*/
	~data();	/*	Deconstructor	*/

	unsigned int _iterator0 = 0;

private:
	std::string filename = "C:\\Files\\log.txt";	/*	String storing path to file with it's name.	*/
	std::fstream f_data;							/*	Variable of file stream.	*/

	void getLine();
	void sort1();
	void sort2();
	
public:
	stats *storedStats;			/*	Dynamically allocated array of struct with popular Files.	*/
	stats *storedStats1;
	stats temp;

	int *views = 0;
};

//************************************************ METHODS ************************************************

void data::sort1()
{
	new_debug::reset();
	new_debug::debug();
	new_debug::debug("Sort1:");


	for (int sort1Iterator = 0; sort1Iterator < _iterator0; sort1Iterator++)
	{
		if (storedStats[sort1Iterator].views < storedStats[sort1Iterator + 1].views)
		{
			temp = storedStats[sort1Iterator];
			storedStats[sort1Iterator] = storedStats[sort1Iterator + 1];
			storedStats[sort1Iterator + 1] = temp;

			new_debug::debug(storedStats[sort1Iterator].filename, storedStats[sort1Iterator].pc_id, storedStats[sort1Iterator].views);
			new_debug::debug(storedStats[sort1Iterator + 1].filename, storedStats[sort1Iterator + 1].pc_id, storedStats[sort1Iterator + 1].views);
			new_debug::debugIterator++;
			new_debug::debug("Debug iterator: ", new_debug::debugIterator);
			new_debug::debug("Sort iterator: ", sort1Iterator);
			new_debug::debug("end");
			new_debug::debug();

			sort1Iterator = -1;
		}

		else
		{
			new_debug::debug(storedStats[sort1Iterator].filename, storedStats[sort1Iterator].pc_id, storedStats[sort1Iterator].views);
			new_debug::debugIterator++;
			new_debug::debug("end");
			new_debug::debug();
		}
	}

}

void data::sort2()
{
	new_debug::reset();
	new_debug::debug();
	new_debug::debug("Sort2:");

	storedStats1 = new stats[_iterator0];

	for (int quickIterator = 0; quickIterator < _iterator0; quickIterator++)
	{
		storedStats1[quickIterator] = storedStats[quickIterator];
	}


	for (int sort2Iterator = 0; sort2Iterator < _iterator0; sort2Iterator++)
	{
		if (storedStats1[sort2Iterator].pc_id < storedStats1[sort2Iterator + 1].pc_id)
		{
			temp = storedStats1[sort2Iterator];
			storedStats1[sort2Iterator] = storedStats1[sort2Iterator + 1];
			storedStats1[sort2Iterator + 1] = temp;

			new_debug::debug(storedStats1[sort2Iterator].filename, storedStats1[sort2Iterator].pc_id, storedStats1[sort2Iterator].views);
			new_debug::debug(storedStats1[sort2Iterator + 1].filename, storedStats1[sort2Iterator + 1].pc_id, storedStats1[sort2Iterator + 1].views);
			new_debug::debugIterator++;
			new_debug::debug("Debug iterator: ", new_debug::debugIterator);
			new_debug::debug("Sort iterator: ", sort2Iterator);
			new_debug::debug("end");
			new_debug::debug();

			sort2Iterator = -1;
		}

		else
		{
			new_debug::debug(storedStats1[sort2Iterator].filename, storedStats1[sort2Iterator].pc_id, storedStats1[sort2Iterator].views);
			new_debug::debugIterator++;
			new_debug::debug("end");
			new_debug::debug();
		}
	}

}

void data::getLine()
{
	std::string *trash = new std::string;	/*	String collecting the trash from begining of file
												!!! NOTE !!! File must begin with space!	*/
	new_debug::debug();
	new_debug::debug("Trash:");

	f_data >> *trash;	/*	Trash collector	*/
							
	new_debug::debug(*trash);

	new_debug::debug();
	new_debug::debug("Iterator:");

	f_data >> _iterator0;			/*	String collecting the size of log file
										!!! NOTE !!! After space must appear size of file!	*/

	new_debug::debug(_iterator0);
	
	storedStats = new stats[_iterator0];	/*	Creating dynamically allocated array of structs	*/
	
	new_debug::debug();
	new_debug::debug("GetLine:");
	
	for (int i = 0; i < _iterator0; i++)
	{
		f_data >> storedStats[i].filename >> storedStats[i].pc_id >> storedStats[i].views;

		new_debug::debug(storedStats[i].filename, storedStats[i].pc_id, storedStats[i].views);

	}
}

void data::print_records1()
{
	unsigned int iterator0;
	
	std::cout << "views\tpc_id\tfilename\n" 
		<< "---------------------------------------------" << std::endl;
	
	for (iterator0 = 0; iterator0 < _iterator0; iterator0++)
	{
		std::cout << storedStats[iterator0].views << " \t"
			<< storedStats[iterator0].pc_id << " \t"
			<< storedStats[iterator0].filename << std::endl;
	}
}

void data::print_records1(int max)
{
	
	unsigned int iterator0;
	
	std::cout << "views\tpc_id\tfilename\n" 
		<< "---------------------------------------------" << std::endl;
	for (iterator0 = 0; iterator0 < max; iterator0++)
	{
		std::cout << storedStats[iterator0].views << " \t"
			<< storedStats[iterator0].pc_id << " \t"
			<< storedStats[iterator0].filename << std::endl;
	}
}

void data::print_records2()
{
	unsigned int iterator0;
	std::cout << "pc_id\tfilename\tviews\n" 
		<< "---------------------------------------------" << std::endl;

	for (iterator0 = 0; iterator0 < _iterator0; iterator0++)
	{
		if ((iterator0-1) >= 0)
		{
			if (storedStats1[iterator0].pc_id != storedStats1[iterator0-1].pc_id)
			{
				std::cout << "\n " << storedStats1[iterator0].pc_id << " \t";
			}

			else std::cout << " \t";
		}

		std::cout << storedStats1[iterator0].views << " \t" << storedStats1[iterator0].filename << std::endl;
	}
}

data::data()
{
	std::cout << "Ensure, that file is located in this localisation:" << std::endl;
	std::cout << "C:\\Files\\log.txt" << std::endl;
	f_open();
	getLine();
}

data::~data()
{
	f_close();
}

void data::f_open()
{
	f_data.open(filename, std::ios::in);
	if (f_data.is_open() == true)
	{
		std::cout << "File opened" << std::endl;
	}
}

void data::f_close()
{
	f_data.close();
}

void data::setup()
{
	sort1();
	sort2();
}

//************************************************ MAIN ************************************************

int main()
{
	data rank;

	rank.setup();

	char choice;
	int i_choice;

back1:
	system("cls");
	std::cout << "What do you want to see?\n"
		<< "1. Sorted by Most Views\n"
		<< "2. Sorted by PC ID" << std::endl;
	std::cin >> choice;

	if (choice == '1')
	{
	back2:
		system("cls");
		std::cout << "What do you want to see?\n"
			<< "1. All records\n"
			<< "2. Only few" << std::endl;
		std::cin >> choice;

		if (choice == '1')
		{
			system("cls");
			rank.print_records1();
		}

		else if (choice == '2')
		{
		back4:
			system("cls");
			std::cout << "How many?" << std::endl;
			std::cin >> i_choice;

			if (!(i_choice > rank._iterator0))
			{
				rank.print_records1(i_choice);
			}

			else goto back4;
		}

		else goto back2;
	}

	else if (choice == '2')
	{
		system("cls");
		rank.print_records2();
	}
	else goto back1;

	std::cout << std::endl;
	system("pause");
	return 0;
}
